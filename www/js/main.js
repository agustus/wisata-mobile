/* 
* @Author: Dika
* @Date:   2016-06-04 20:57:23
* @Last Modified by:   Dika
* @Last Modified time: 2016-06-07 06:57:49
*/

var appController = angular.module('starter.controllers', []);
var appDirectives = angular.module('starter.directives', []);
var appServices = angular.module('starter.services', []);

appDirectives.directive('onErrorSrc', function() {
    return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.onErrorSrc) {
              attrs.$set('src', attrs.onErrorSrc);
            }
          });
        }
    }
});