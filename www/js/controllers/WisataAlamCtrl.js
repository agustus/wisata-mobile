/* 
* @Author: Dika
* @Date:   2016-06-04 17:35:36
* @Last Modified by:   Dika
* @Last Modified time: 2016-06-11 14:09:20
*/

appController.controller("WisataAlamCtrl", function($scope, SETTINGS, wisataAlamServices, $ionicLoading, $timeout,$compile){

	// tampil loading
    $ionicLoading.show({
      content : 'Loading',
      animation: 'fade-in',
      showBackdrop : true,
      maxWidth: 200,
      showDelay: 0
    });

    $scope.title = "Wisata Alam";
    $scope.page = "alam";

    $scope.doRefresh = function(){
    	$scope.data();
    	$scope.$broadcast('scroll.refreshComplete');
    }

	$scope.data = function(){
		wisataAlamServices.ambilSemua().success(function(res){
			$scope.datana = res;
			$ionicLoading.hide();
		}).error(function(err){
			console.log(err);
			$timeout(function(){
				$ionicLoading.hide();
			},2000);
		});
	}

	
});

appController.controller("DetailWisataAlamCtrl", function(
	$scope, $state, $ionicLoading, $timeout, $stateParams, wisataAlamServices, $sce, $ionicPopup, $ionicModal, 
	$ionicSlideBoxDelegate, $ionicScrollDelegate, $document, $compile, $cordovaGeolocation, $ionicPlatform, $localstorage, globalServices
){
	// tampil loading
    $scope.loading = function(){
    	$ionicLoading.show({
			content : 'Loading',
			animation: 'fade-in',
			showBackdrop : true,
			maxWidth: 200,
			showDelay: 0
		});
    };

	// ambil akun dari localstorage
	var userInfo = $localstorage.getObject('userLogin');
	$scope.userInfo = userInfo;
	console.log(userInfo);

    // title
    $scope.title = "Wisata Alam";
    $scope.page = "alam";

    // Rating
    $scope.click = function (param) {

    	if(userInfo.isLogin==1){
    		globalServices.cekAkun({fbid : userInfo.fbid}).success(function(res){

    			wisataAlamServices.rating({userid : res.user_id, wisata_id : $stateParams.wisataId, jumRating: param}).success(function(resRate){
					console.log(resRate);
					if(resRate){
						$scope.data();
						console.log("berhasil!");
						// $ionicLoading.hide();
					}
				}).error(function(err){
					console.log(err);
					// $ionicLoading.hide();
				});
    		}).error(function(err){
				console.log(err);
				$ionicLoading.hide();
			});
    	}else{
    		var myPopup = $ionicPopup.confirm({
				template: '<center>Maaf, kamu belum masuk ke akun kamu sekarang.<center>',
				title: 'Maaf',
				scope: $scope,
				buttons: [{ 
					text: 'Batal',
					type: 'button-default',
					onTap: function(e) {

					}
				}, {
					text: 'Masuk',
					type: 'button-dark',
					onTap: function(e) {
						$state.go('app.akun');
						console.log("masuk");
						return true;
					}
				}]
			});
    	}
        
    };

	/* Create the galeri modal that we will use later */
	$scope.images = [];
	$scope.loadGaleri = function(){
		wisataAlamServices.ambilSatuGaleri($stateParams.wisataId).success(function(res){
			console.log(res);
			for(var i = 0; i < res.length; i++) {
				$scope.images.push({id: i, src: res[i].url_foto, msg: res[i].nama_foto, wisata_id: res[i].wisata_id});
			}
		}).error(function(err){
			console.log(err);
		});
	}

	$ionicModal.fromTemplateUrl('templates/wisataAlam/galeri.html', {
		scope: $scope
	}).then(function(modalGaleri) {
		$scope.modalGaleri = modalGaleri;
	});
	$scope.openGaleri = function(){
		$scope.modalGaleri.show();
	}
	$scope.closeGaleri = function(){
		$scope.modalGaleri.hide();
	}
	/* End of modal galeri */

	/* Start modal map */
	$ionicModal.fromTemplateUrl('templates/wisataAlam/map.html', {
		scope: $scope
	}).then(function(modalMap) {
		$scope.modalMap = modalMap;
	});
	$scope.openMap = function(){
		$scope.modalMap.show();
	}
	$scope.closeMap = function(){
		$scope.modalMap.hide();
	}
	/* End of map */

	/* Start modal komntar */
	$scope.tampilKomentar = function(){
		wisataAlamServices.ambilKomentar($stateParams.wisataId).success(function(res){
			$scope.komentar = res;
		});
		$scope.$broadcast('scroll.refreshComplete');
	};

	$scope.allowKomentar = (userInfo.isLogin==1) ? true : false;

	$scope.masukKomentar = function(){
		$state.go('app.akun');
		$scope.modalKomentar.hide();
	};

	$scope.hapusKomentar = function(komentar_alam_id,fb_id){
		var myPopup = $ionicPopup.confirm({
			template: '<center>Apakah komentar ini akan di hapus?<center>',
			title: 'Konfirmasi',
			scope: $scope,
			buttons: [{
				text: 'Batal',
				type: 'button-default',
				onTap: function(e) {

				}
			},{
				text: 'Hapus',
				type: 'button-assertive',
				onTap: function(e) {
					console.log("hapus");
					$scope.loading();
					wisataAlamServices.hapusKomentar({komentar_id : komentar_alam_id}).success(function(resHap){
						console.log(resHap);
						if(resHap.status){
							$scope.tampilKomentar();
						}
						$ionicLoading.hide();
					}).error(function(err){
						console.log(err);
						$ionicLoading.hide();
					});
					return true;
				}
			}]
		});
	};

	$scope.kirimKomentar = function(komentar){
		
		if(komentar.pesan!=undefined && komentar.pesan!=' ' && komentar.pesan.length>5){
			$scope.loading();

			console.log(komentar.pesan);
			globalServices.cekAkun({fbid : userInfo.fbid}).success(function(res){
				console.log(res);
				wisataAlamServices.kirimKomentar({userid : res.user_id, wisata_id : $stateParams.wisataId, komentar: komentar.pesan}).success(function(resKom){
					console.log(resKom);
					if(resKom){
						console.log("berhasil!");
						$scope.tampilKomentar();
						$ionicLoading.hide();
					}
				}).error(function(err){
					console.log(err);
					$ionicLoading.hide();
				});

			}).error(function(err){
				console.log(err);
				$ionicLoading.hide();
			});
		}
	};

	$ionicModal.fromTemplateUrl('templates/wisataAlam/komentar.html', {
		scope: $scope
	}).then(function(modalKomentar) {
		$scope.modalKomentar = modalKomentar;
	});
	$scope.openKomentar = function(){
		$scope.modalKomentar.show();
	}
	$scope.closeKomentar = function(){
		$scope.modalKomentar.hide();
	}
	/* End of komntar */

	// Modal open image
	$scope.zoomMin = 1;

	$scope.showImages = function(index) {
		console.log(index);
		$scope.activeSlide = index;
		$scope.showModal('templates/galeri/detail.html');
	};

	$scope.showModal = function(templateUrl) {
		$ionicModal.fromTemplateUrl(templateUrl, {
			scope: $scope
		}).then(function(modal) {
			$scope.modal = modal;
			$scope.modal.show();
		});
	}

	$scope.closeModal = function() {
		$scope.modal.hide();
		$scope.modal.remove()
	};

	$scope.updateSlideStatus = function(slide) {
		var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
		if (zoomFactor == $scope.zoomMin) {
			$ionicSlideBoxDelegate.enableSlide(true);
		} else {
			$ionicSlideBoxDelegate.enableSlide(false);
		}
	};

    // end of modal image

    $scope.doRefresh = function(){
    	$scope.data();
    	$scope.$broadcast('scroll.refreshComplete');
    }

	$scope.data = function(){
		$scope.loading();

		$scope.nilai = 0;
		wisataAlamServices.ambilSatu($stateParams.wisataId).success(function(res){
			if(res==null){
				$state.go("app.alam");
			}

			$scope.trust = $sce.trustAsHtml;
			$scope.title = res.nama_wisata;
			$scope.datana = res;
			$scope.nilai = res.nilai;
			$ionicLoading.hide();
		}).error(function(err){
			console.log(err);
			$timeout(function(){
				$ionicLoading.hide();
			},2000);
		});
	}

	/* Map */
	var lat1 = -6.8677085;
	var long1 = 107.5649633;
	var lat2 = -6.8731622;
	var long2 = 107.5682678;	

	// map object
	$scope.map = {
		control: {},
		center: {
		latitude: lat1,
		longitude: long1
	},
		zoom: 14
	};

	// marker object
	$scope.marker = {
		center: {
			latitude: lat1,
			longitude: long1
		}
	}

	// instantiate google map objects for directions

	var directionsDisplay = new google.maps.DirectionsRenderer();
	var directionsService = new google.maps.DirectionsService();
	var geocoder = new google.maps.Geocoder();


	// directions object -- with defaults
	wisataAlamServices.ambilSatu($stateParams.wisataId).success(function(res){
		if(res==null){
			$state.go("app.alam");
		}
		// tampil loading
	    $ionicLoading.show({
			content : 'Loading',
			animation: 'fade-in',
			showBackdrop : true,
			maxWidth: 200,
			showDelay: 0
		});
		// eusi map
		$ionicPlatform.ready(function() {
			$scope.ambilDireksi = function() {
				$ionicLoading.show({
					content: 'Sedang mengambil posisi...',
					showBackdrop: false
				});

				var posOptions = {timeout: 10000, enableHighAccuracy: false};
				$cordovaGeolocation.getCurrentPosition(posOptions)
				.then(function (position) {
					$ionicLoading.hide();
					// alert(position.coords.latitude+" - "+position.coords.longitude);
					$scope.directions = {
						origin: position.coords.latitude+","+position.coords.longitude,
						destination: res.lat+","+res.long,
						showList: false
					}

					var request = {
						origin: $scope.directions.origin,
						destination: $scope.directions.destination,
						travelMode: google.maps.DirectionsTravelMode.DRIVING
					};
					
					directionsService.route(request, function (response, status) {
						if (status === google.maps.DirectionsStatus.OK) {
							directionsDisplay.setDirections(response);
							directionsDisplay.setMap($scope.map.control.getGMap());
							directionsDisplay.setPanel(document.getElementById('directionsList'));
							$scope.directions.showList = true;
							console.log(response.routes[0].legs[0].distance.text+' '+response.routes[0].legs[0].duration.text);
						} else {
							alert('Google route unsuccesfull!');
						}
					});
					console.log(position);
				}, function(err) {
					$ionicLoading.hide();
					console.log(err);
					alert(err);
				});


			}
		});


		$ionicLoading.hide();
	}).error(function(err){
		// error
		console.log(err);
		$timeout(function(){
			$ionicLoading.hide();
		},2000);
	});

	/* End of Map */

});