/* 
* @Author: Dika
* @Date:   2016-06-11 19:57:47
* @Last Modified by:   Dika
* @Last Modified time: 2016-06-12 23:00:53
* @Catatan : File upload from https://forum.ionicframework.com/t/working-example-to-upload-photo-from-camera-or-galley-with-ngcordova/12852
*/

appController.controller('UsulanCtrl', function ($scope, $ionicPopup, $timeout, $cordovaCamera, $ionicLoading, $localstorage, globalServices, SETTINGS, $timeout) {
	$scope.$on('$ionicView.beforeEnter', function (event, viewData) {
		viewData.enableBack = false;
	});

	// Pertama load di halaman akun
	$scope.$on('$ionicView.enter', function(){
		// ambil akun dari localstorage
		var userInfo = $localstorage.getObject('userLogin');
		$scope.userInfo = userInfo;
		
		$timeout(function() {
			$scope.allowUsulan = (userInfo.isLogin==1) ? true : false;
			$scope.data();
		}, 10);
	});

	// Loading
	$scope.loading = function(){
		$ionicLoading.show({
			content : 'Loading',
			animation: 'fade-in',
			showBackdrop : true,
			maxWidth: 200,
			showDelay: 0
		});
	};

	$scope.alert = function(title,deskripsi){
		$timeout(function () {
			var alertPopup = $ionicPopup.alert({
				title : title,
				template : deskripsi
			});
		},100);
	};

	$scope.data = function(){
		$scope.loading();
		var userInfo = $localstorage.getObject('userLogin');
		globalServices.cekAkun({fbid : userInfo.fbid}).success(function(resAkun){
			globalServices.daftarUsulan({user_id:resAkun.user_id}).success(function(res){
				$scope.dataUsulan = res;
				$ionicLoading.hide();
			}).error(function(err){
				console.log(err);
				$ionicLoading.hide();
			});
		});
	};

	$scope.refresh = function(){
		$scope.data();
		$scope.$broadcast('scroll.refreshComplete');
	};

	$scope.hapusWisata = function(idwisata, tipe){
		var myPopup = $ionicPopup.confirm({
			template: '<center>Apakah wisata ini akan di hapus?<center>',
			title: 'Konfirmasi',
			scope: $scope,
			buttons: [{
				text: 'Batal',
				type: 'button-default',
				onTap: function(e) {

				}
			},{
				text: 'Hapus',
				type: 'button-assertive',
				onTap: function(e) {
					$scope.loading();
					globalServices.hapusUsulan({wisata_id: idwisata, tipe:tipe}).success(function(res){
						$scope.data();
						$scope.alert('Perhatian!','Usulan wisata telah berhasil di hapus.');
						$ionicLoading.hide();
					}).error(function(err){
						console.log(err);
						$ionicLoading.hide();
					});
					return true;
				}
			}]
		});
	};

});

appController.controller('KirimUsulanCtrl', function ($scope, $state, $ionicPlatform, $cordovaCamera, $ionicLoading, $localstorage, globalServices, $ionicPopup, SETTINGS, $timeout, $cordovaFileTransfer) {
	$scope.data = { "ImageURI" :  "Select Image" };

	$scope.alert = function(title,deskripsi){
		$timeout(function () {
			var alertPopup = $ionicPopup.alert({
				title : title,
				template : deskripsi
			});
		},100);
	};

	// Loading
	$scope.loading = function(){
		$ionicLoading.show({
			content : 'Loading',
			animation: 'fade-in',
			showBackdrop : true,
			maxWidth: 200,
			showDelay: 0
		});
	};

	// Pertama load di halaman akun
	$scope.$on('$ionicView.enter', function(){
		// ambil akun dari localstorage
		var userInfo = $localstorage.getObject('userLogin');
		$scope.userInfo = userInfo;
		
		$timeout(function() {
			$scope.allowUsulan = (userInfo.isLogin==1) ? true : false;
		}, 10);
	});

	
	$scope.usulan = {};

	// Pilihan Status
    $scope.statuses = [{
        name: 'Wisata Alam',
        value: 'alam'
    },{
        name: 'Wisata Kuliner',
        value: 'kuliner'
    }];
    $scope.usulan.tipe = 'alam'; // Selected status 

	$scope.kirim = function(data){
		$scope.loading();
		if(data.nama==undefined || data.alamat==undefined || data.deskripsi==undefined){
			$scope.alert('Perhatian!','Tidak boleh ada data yang kosong.');
			$timeout(function(){
				$ionicLoading.hide();
			},200);
		}else{
			var d = new Date();
			var userInfo = $localstorage.getObject('userLogin');
			var nama=data.nama;
			var alamat=data.alamat;
			var deskripsi=data.deskripsi;
			var foto = ($scope.picData!=undefined) ? "usul_"+Math.floor((Math.random() * d) + 1) : '';

			// cek akun (ngambil user id by fb id dari localstorage)
			globalServices.cekAkun({fbid : userInfo.fbid}).success(function(resAkun){
				// kirim usulan
				globalServices.kirimUsulan({
					nama_wisata 	: nama,
					alamat_wisata	: alamat,
					deskripsi		: deskripsi,
					user_id			: resAkun.user_id,
					tipe 			: data.tipe,
					foto 			: foto,
				}).success(function(resKirim){
					if(resKirim.status){
						// alert(foto);

						// upload foto
						$ionicPlatform.ready(function() {
							if($scope.picData!=undefined){
								$scope.uploadPicture(foto);
							}
						});
						
						$scope.usulan.nama = "";
						$scope.usulan.alamat = "";
						$scope.usulan.deskripsi = "";
						$scope.picData = undefined;
						foto = "usul_"+Math.floor((Math.random() * d) + 1);
						$scope.alert('Berhasil!','Usulan telah terkirim.');
						$state.go('app.usulan');
					}
					console.log(resKirim);
					$timeout(function(){
						$ionicLoading.hide();
					},200);
				}).error(function(err){
					console.log(err);
					$timeout(function(){
						$ionicLoading.hide();
					},200);
				});
			}).error(function(err){
				console.log(err);
				$timeout(function(){
					$ionicLoading.hide();
				},200);
			});
		}
		
	};

	// Ambil foto dari kamera
	$scope.takePicture = function() {
		var options = {
			quality: 50,
			destinationType: Camera.DestinationType.FILE_URL,
			sourceType: Camera.PictureSourceType.CAMERA
		};
		$cordovaCamera.getPicture(options).then(
			function(imageData) {
				$scope.picData = imageData;
				$scope.ftLoad = true;
				$localstorage.set('fotoUp', imageData);
				$ionicLoading.show({template: 'Menangkap foto..', duration:500});
				// alert(imageData);
			},
			function(err){
				$ionicLoading.show({template: 'Kesalahan pemuatan...', duration:500});
			}
		);
		$timeout(function(){
			$ionicLoading.hide();
		},200);
	}
	// Ambil foto dari galeri
	$scope.selectPicture = function() { 
		var options = {
			quality: 50,
			destinationType: Camera.DestinationType.FILE_URI,
			sourceType: Camera.PictureSourceType.PHOTOLIBRARY
		};

		$cordovaCamera.getPicture(options).then(
			function(imageData) {
				$scope.picData = imageData;
				$scope.ftLoad = true;
				$localstorage.set('fotoUp', imageData);
				$ionicLoading.show({template: 'Foto diambil...', duration:500});
				// alert(imageData);
			},
			function(err){
				$ionicLoading.show({template: 'Kesalahan pemuatan...', duration:500});
			}
		);

		$timeout(function(){
			$ionicLoading.hide();
		},200);
	};
	// upload foto
	$scope.uploadPicture = function(namaFoto) {
		$ionicLoading.show({template: 'Mengirim foto...'});
		var fileURL = $scope.picData;
		var options = {};

		options.fileKey = "file";
		options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
		options.mimeType = "image/jpeg";
		options.chunkedMode = true;

		var params = {};
		params.folder = "assets/upload/usul";
		params.namaFile = namaFoto;

		options.params = params;

		document.addEventListener('deviceready', function () {
			$cordovaFileTransfer.upload(encodeURI(SETTINGS.baseurl+"upload.php"), fileURL, options).then(
			function(result){
				// Success!
				// alert("Sukses");
				console.log("SUCCESS: " + JSON.stringify(result.response));
			}, function(err) {
				$ionicLoading.show({template: 'Kesalahan saat mengirim foto ke server...', duration:500});
				console.log("ERROR: " + JSON.stringify(err));
			}, function (progress) {
				// constant progress updates
			});
		}, false);

		$timeout(function(){
			$ionicLoading.hide();
		},200);
	}

	var viewUploadedPictures = function() {
		// $ionicLoading.show({template: 'Loading foto...'});
        server = SETTINGS.baseurl+"upload.php";
        if (server) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange=function(){
            if(xmlhttp.readyState === 4){
                    if (xmlhttp.status === 200) {					 
                document.getElementById('server_images').innerHTML = xmlhttp.responseText;
                    }
                    else { $ionicLoading.show({template: 'Error', duration: 1000});
					return false;
                    }
                }
            };
            xmlhttp.open("GET", server , true);
            xmlhttp.send()}	;
		$ionicLoading.hide();
    }
});