/* 
* @Author: Dika
* @Date:   2016-05-06 10:44:44
* @Last Modified by:   Dika
* @Last Modified time: 2016-06-04 23:20:34
*/

appController.controller("MapSekitarCtrl", function($scope, $ionicLoading, $ionicPlatform, $cordovaGeolocation, $compile, $timeout){

	// deklarasi kordinat
	var lat1 = -6.8677085;
	var long1 = 107.5649633;

	$ionicPlatform.ready(function() {
		$ionicLoading.show({
			content: 'Getting current location...',
			showBackdrop: false
		});

		var posOptions = {timeout: 10000, enableHighAccuracy: false};

		$cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
			$ionicLoading.hide();

			lat1 = position.coords.latitude;
			long1 = position.coords.longitude;

		}, function(err) {
			$ionicLoading.hide();
			console.log(err);
			alert(err);
		});

		
	});

	console.log(lat1+'-'+long1);

	// ambil lokasi sekarang
	// navigator.geolocation.getCurrentPosition(function(pos) {
	// 	$scope.map = {
	// 		control: {},
	// 		center: {
	// 			latitude: pos.coords.latitude,
	// 			longitude: pos.coords.longitude
	// 		},
	// 		zoom: 14
	// 	};
	// }, function(error) {
	// 	alert('Unable to get location: ' + error.message);
	// });

	// map object
	$scope.mapWisata = {
		control: {},
		center: {
			latitude: lat1,
			longitude: long1
		},
		zoom: 14
	};

	// marker object
	$scope.markerWisata = {
		center: {
			latitude: lat1,
			longitude: long1
		}
	}

})

.controller("DetailMapCtrl", function($scope, $state, $stateParams){
	console.log($stateParams.mapId);
})

;