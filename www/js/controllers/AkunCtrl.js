/* 
* @Author: Dika
* @Date:   2016-06-07 18:42:31
* @Last Modified by:   Dika
* @Last Modified time: 2016-06-12 16:46:17
*/

appController.controller("AkunCtrl", function($scope, $ionicModal, $timeout, ngFB, $localstorage, globalServices, $ionicLoading, $timeout){
	// hide back menu
	$scope.$on('$ionicView.beforeEnter', function (event, viewData) {
		viewData.enableBack = false;
	});

	$scope.tombol = true;

	// ambil akun dari localstorage
	var userInfo = $localstorage.getObject('userLogin');
		
	if(userInfo.isLogin==0 || userInfo.isLogin==undefined){
		$localstorage.setObject('userLogin', {
			isLogin : 0,
			fbid : null,
			nama : null
		});
	}else{
		console.log(userInfo);
		$scope.tombol = false;
		$scope.user = {
			id : userInfo.fbid,
			name : userInfo.nama
		};
	}

	// Loading setelah login
	$scope.loading = function(){
		$ionicLoading.show({
			content : 'Loading',
			animation: 'fade-in',
			showBackdrop : true,
			maxWidth: 200,
			showDelay: 0
		});
	};

	$scope.fbLogin = function () {
		$scope.loading();
		ngFB.login({scope: 'email,read_stream,publish_actions'}).then(
		function (response) {
			if(response.status === 'connected'){
				console.log('Facebook login succeeded');

				ngFB.api({
					path: '/me',
					params: {fields: 'id,name'}
				}).then(
				function (user) {
					$scope.user = user;

					// simpan data login fb ke localstorage
					$localstorage.setObject('userLogin', {
						isLogin : 1,
						fbid : user.id,
						nama : user.name
					});

					globalServices.fbLogin({fbid : user.id, nama : user.name}).success(function(resultLog){
						if(resultLog.status){
							console.log("Akun baru");
						}else{
							console.log("Akun lama");
						}

						$timeout(function(){
							$ionicLoading.hide();
						}, 2000);
					});

				},
				function (error) {
					alert('Facebook error: ' + error.error_description);
					$timeout(function(){
						$ionicLoading.hide();
					}, 2000);
				});

				$scope.tombol = false;
				$scope.closeLogin();
				$timeout(function(){
					$ionicLoading.hide();
				}, 2000);
			}else{
				$timeout(function(){
					$ionicLoading.hide();
				}, 2000);
				alert('Facebook login failed');
			}
		});
	};

	$scope.logout = function(){
		$scope.loading();
		$localstorage.setObject('userLogin', {
			isLogin : 0,
			fbid : null,
			nama : null
		});
		$scope.tombol = true;
		$timeout(function(){
			$ionicLoading.hide();
		}, 2000);
	};

});