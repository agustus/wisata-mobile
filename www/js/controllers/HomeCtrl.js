/* 
* @Author: Dika
* @Date:   2016-05-05 21:55:05
* @Last Modified by:   Dika
* @Last Modified time: 2016-06-11 19:14:52
*/

appController.controller("HomeCtrl", function($scope, $ionicLoading, $compile, $timeout, $ionicLoading, wisataKulinerServices, wisataAlamServices){
	// tampil loading
    $scope.loading = function(){
    	$ionicLoading.show({
			content : 'Loading',
			animation: 'fade-in',
			showBackdrop : true,
			maxWidth: 200,
			showDelay: 0
		});
    };

	$scope.data = function(){
		$scope.loading();

		wisataKulinerServices.topWisata().success(function(resKuliner){
			$scope.dataWisataKuliner = resKuliner;
		}).error(function(err){
			console.log(err);
		});

		wisataAlamServices.topWisata().success(function(resAlam){
			$scope.dataWisataAlam = resAlam;
		}).error(function(err){
			console.log(err);
		});

		$timeout(function() {
			$ionicLoading.hide();
		}, 2000);
	};

	$scope.doRefresh = function(){
    	$scope.data();
    	$scope.$broadcast('scroll.refreshComplete');
    }

	// function initialize() {
 //        var myLatlng = new google.maps.LatLng(43.07493,-89.381388);
        
 //        var mapOptions = {
 //          center: myLatlng,
 //          zoom: 16,
 //          mapTypeId: google.maps.MapTypeId.ROADMAP
 //        };
 //        var map = new google.maps.Map(document.getElementById("map"),
 //            mapOptions);
        
 //        //Marker + infowindow + angularjs compiled ng-click
 //        var contentString = "<div><a ng-click='clickTest()'>Click me!</a></div>";
 //        var compiled = $compile(contentString)($scope);

 //        var infowindow = new google.maps.InfoWindow({
 //          content: compiled[0]
 //        });

 //        var marker = new google.maps.Marker({
 //          position: myLatlng,
 //          map: map,
 //          title: 'Uluru (Ayers Rock)'
 //        });

 //        google.maps.event.addListener(marker, 'click', function() {
 //          infowindow.open(map,marker);
 //        });

 //        $scope.map = map;
 //      }

 //      google.maps.event.addDomListener(window, 'load', initialize);
      
 //      $scope.centerOnMe = function() {
 //        if(!$scope.map) {
 //          return;
 //        }

 //        $scope.loading = $ionicLoading.show({
 //          content: 'Getting current location...',
 //          showBackdrop: false
 //        });

 //        navigator.geolocation.getCurrentPosition(function(pos) {
 //          $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
 //          $scope.loading.hide();
 //        }, function(error) {
 //          alert('Unable to get location: ' + error.message);
 //        });
 //      };
      
 //      $scope.clickTest = function() {
 //        alert('Example of infowindow with ng-click')
 //      };
	
})

;