// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', [
  'ionic', 
  'ngCordova',

  'starter.controllers',
  'starter.services',
  'starter.directives',

  'google-maps',
  'ngOpenFB'
])

.constant('SETTINGS', {
  appId_fb  : '273571086008542',
  vesion  : '0.0.1',
  // baseurl : 'http://192.168.1.100/golembang/',
  baseurl : 'http://wisata.dika.web.id/',
  // apiurl  : 'http://192.168.1.100/golembang/index.php/api/'
  apiurl  : 'http://wisata.dika.web.id/index.php/api/'
  // apiurl  : 'http://192.168.56.2/golembang/index.php/api/'
  // apiurl  : 'http://192.168.42.148/golembang/index.php/api/'
})

.run(function($ionicPlatform, ngFB, SETTINGS, globalServices, $state, $localstorage) {

  ngFB.init({appId: SETTINGS.appId_fb});

  $ionicPlatform.ready(function() {
      if(window.Connection) {
          if(navigator.connection.type == Connection.NONE) {
              $ionicPopup.confirm({
                  title: "Internet Disconnected",
                  content: "The internet is disconnected on your device."
              })
              .then(function(result) {
                  if(!result) {
                      ionic.Platform.exitApp();
                  }
              });
          }
      }
  });

  globalServices.cekKoneksi().success(function(){
    
  }).error(function(){
    console.log("Tidak ada koneksi ke server");
    $state.go('app.koneksi');
  });

  // $localstorage.setObject('userLogin', {
  //   isLogin : 0,
  //   fbid : null,
  //   nama : null
  // });

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.tabs.position('top'); //bottom

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl'
      }
    }
  })

  .state('app.akun', {
    url: '/akun',
    views: {
      'menuContent': {
        templateUrl: 'templates/akun.html',
        controller: 'AkunCtrl'
      }
    }
  })

  .state('app.usulan', {
    url: '/usulan',
    views: {
      'menuContent': {
        templateUrl: 'templates/usulan/list.html',
        controller: 'UsulanCtrl'
      }
    }
  })

  .state('app.kirimUsulan', {
    url: '/usulan/kirim',
    views: {
      'menuContent': {
        templateUrl: 'templates/usulan/form.html',
        controller: 'KirimUsulanCtrl'
      }
    }
  })

  .state('app.galeri', {
    url: '/galeri',
    views: {
      'menuContent': {
        templateUrl: 'templates/galeri/list.html',
        controller: 'GaleriCtrl'
      }
    }
  })

  .state('app.kuliner', {
    url: '/wisata/kuliner',
    views: {
      'menuContent': {
        templateUrl: 'templates/wisataAlam/list.html',
        controller: 'WisataKulinerCtrl'
      }
    }
  })

  .state('app.detailKuliner', {
    url: '/wisata/kuliner/:wisataId',
    views: {
      'menuContent': {
        templateUrl: 'templates/wisataAlam/detail.html',
        controller: 'DetailWisataKulinerCtrl'
      }
    }
  })

  .state('app.alam', {
    url: '/wisata/alam',
    views: {
      'menuContent': {
        templateUrl: 'templates/wisataAlam/list.html',
        controller: 'WisataAlamCtrl'
      }
    }
  })

  .state('app.detailAlam', {
    url: '/wisata/alam/:wisataId',
    views: {
      'menuContent': {
        templateUrl: 'templates/wisataAlam/detail.html',
        controller: 'DetailWisataAlamCtrl'
      }
    }
  })

  .state('app.map', {
    url: '/map',
    views: {
      'menuContent': {
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      }
    }
  })

  .state('app.detailMap', {
    url: '/map/detail/:mapId',
    views: {
      'menuContent': {
        templateUrl: 'templates/map/detail.html',
        controller: 'DetailMapCtrl'
      }
    }
  })

  .state('app.sekitar', {
    url: '/map/sekitar',
    views: {
      'menuContent': {
        templateUrl: 'templates/map/sekitar.html',
        controller: 'MapSekitarCtrl'
      }
    }
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.koneksi', {
      url: '/koneksi',
      views: {
        'menuContent': {
          templateUrl: 'templates/koneksi.html'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
