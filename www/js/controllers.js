appController.controller('GaleriCtrl', function($scope, wisataAlamServices, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate,$ionicLoading){
  $scope.images = [];

  $scope.zoomMin = 1;

  $scope.showImages = function(index) {
    console.log(index);
    $scope.activeSlide = index;
    $scope.showModal('templates/galeri/detail.html');
  };
 
  $scope.showModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }
   
  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  };
   
  $scope.updateSlideStatus = function(slide) {
    var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
    if (zoomFactor == $scope.zoomMin) {
      $ionicSlideBoxDelegate.enableSlide(true);
    } else {
      $ionicSlideBoxDelegate.enableSlide(false);
    }
  };

  // tampil loading
  $ionicLoading.show({
    content : 'Loading',
    animation: 'fade-in',
    showBackdrop : true,
    maxWidth: 200,
    showDelay: 0
  });

  $scope.loadImages = function() {
      wisataAlamServices.ambilGaleri().success(function(res){
        console.log(res);
        for(var i = 0; i < res.length; i++) {
          $scope.images.push({id: i, src: res[i].url_foto, msg: res[i].nama_wisata, wisata_alam_id: res[i].wisata_alam_id});
        }
        $ionicLoading.hide();
      }).error(function(err){
        console.log(err);
        $ionicLoading.hide();
      });
  }
});

appController.controller('AppCtrl', function($scope, $ionicModal, $timeout, $ionicLoading, $ionicHistory, $state, $ionicPopup, globalServices, $localstorage) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  var userInfo = $localstorage.getObject('userLogin');
  $scope.userInfo = userInfo;
  $scope.showMenuAkun = (userInfo.isLogin==1) ? true : false;

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  $scope.exit = function(){
    $ionicPopup.confirm({
        title: "Konfirmasi",
        content: "Apakah anda akan keluar aplikasi?"
    })
    .then(function(result) {
        if(result) {
            ionic.Platform.exitApp();
        }
    });
  };

  $scope.refreshKoneksi = function(){
    // tampil loading
    $ionicLoading.show({
      content : 'Loading',
      animation: 'fade-in',
      showBackdrop : true,
      maxWidth: 200,
      showDelay: 0
    });

    $timeout(function(){
      console.log("Refreshing");
      // cek koneksi ke server
      globalServices.cekKoneksi().success(function(){
        $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: true
        });
        $state.go('app.home');
      });
      $ionicLoading.hide();
      
    },2000);
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope, $ionicPlatform, $cordovaGeolocation, $ionicLoading, $compile) {


  $ionicPlatform.ready(function() {
    $scope.centerOnMe = function() {
      $ionicLoading.show({
        content: 'Getting current location...',
        showBackdrop: false
      });

      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      $cordovaGeolocation.getCurrentPosition(posOptions)
      .then(function (position) {
        $ionicLoading.hide();
        alert(position.coords.latitude+" - "+position.coords.longitude);
        console.log(position);
      }, function(err) {
        $ionicLoading.hide();
        console.log(err);
         alert(err);
      });
    }
  });
  
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
  
})

.controller('MapCtrl', function($scope, $document, $ionicLoading){

  var lat1 = -6.8677085;
  var long1 = 107.5649633;
  var lat2 = -6.8731622;
  var long2 = 107.5682678;

  // map object
$scope.map = {
control: {},
center: {
    latitude: lat1,
    longitude: long1
},
zoom: 14
 };

 // marker object
 $scope.marker = {
 center: {
    latitude: lat1,
    longitude: long1
 }
}

// instantiate google map objects for directions

var directionsDisplay = new google.maps.DirectionsRenderer();
var directionsService = new google.maps.DirectionsService();
var geocoder = new google.maps.Geocoder();


// directions object -- with defaults
$scope.directions = {
origin: lat1+","+long1,
destination: lat2+","+long2,
 showList: false
 }

 // get directions using google maps api
 $scope.getDirections = function () {
  var request = {
  origin: $scope.directions.origin,
  destination: $scope.directions.destination,
  travelMode: google.maps.DirectionsTravelMode.DRIVING
 };
  directionsService.route(request, function (response, status) {
   if (status === google.maps.DirectionsStatus.OK) {
    directionsDisplay.setDirections(response);
    directionsDisplay.setMap($scope.map.control.getGMap());
    directionsDisplay.setPanel(document.getElementById('directionsList'));
    $scope.directions.showList = true;
    console.log(response.routes[0].legs[0].distance.text+' '+response.routes[0].legs[0].duration.text);
  } else {
    alert('Google route unsuccesfull!');
  }
  });
}


})
;
