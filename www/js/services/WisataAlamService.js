/* 
* @Author: Dika
* @Date:   2016-06-04 17:51:09
* @Last Modified by:   Dika
* @Last Modified time: 2016-06-11 19:12:09
*/

appServices.factory('wisataAlamServices', function($http, SETTINGS) {
	var baseUrl = SETTINGS.apiurl;

	return {
		ambilSemua: function(){
			return $http.get(baseUrl+'wisata/ambilSemua'); 
		},
		ambilSatu: function(wisata_id){
			return $http.get(baseUrl+'wisata/ambilSatu/'+wisata_id); 
		},
		ambilGaleri: function(){
			return $http.get(baseUrl+'galeri');
		},
		ambilSatuGaleri: function(wisata_id){
			return $http.get(baseUrl+'wisata/ambilGaleri/'+wisata_id);
		},
		ambilKomentar: function(wisata_id){
			return $http.get(baseUrl+'wisata/ambilKomentar/'+wisata_id);
		},
		kirimKomentar: function(postData){
			return $http.post(baseUrl+'wisata/kirimKomentar', postData);
		},
		hapusKomentar: function(postData){
			return $http.post(baseUrl+'wisata/hapusKomentar', postData);
		},
		rating: function(postData){
			return $http.post(baseUrl+'wisata/tambahRating', postData);
		},
		topWisata: function(){
			return $http.get(baseUrl+'wisata/ambilTopWisata');
		}
	};
});