/* 
* @Author: Dika
* @Date:   2016-06-07 19:46:58
* @Last Modified by:   Dika
* @Last Modified time: 2016-06-12 22:43:02
*/

appServices.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}]);

appServices.factory('globalServices', function($http, SETTINGS) {
	var baseUrl = SETTINGS.apiurl;

	return {
      cekKoneksi: function(){
        return $http.get(baseUrl);
      },
      cekAkun: function(postData){
        return $http.post(baseUrl+"cekAkun", postData);
      },
	    fbLogin: function(postData){
	      return $http.post(baseUrl+"fbLogin", postData);
	    },
      daftarUsulan: function(postData){
        return $http.post(baseUrl+"daftarUsulan", postData);
      },
      kirimUsulan: function(postData){
        return $http.post(baseUrl+"kirimUsulan", postData);
      },
      hapusUsulan: function(postData){
        return $http.post(baseUrl+"hapusUsulan", postData);
      },
	};
});