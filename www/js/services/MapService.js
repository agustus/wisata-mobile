/* 
* @Author: Dika
* @Date:   2016-05-06 21:51:11
* @Last Modified by:   Dika
* @Last Modified time: 2016-06-04 19:35:05
*/


appServices.factory('mapServices', function($ionicPlatform, $cordovaGeolocation, $ionicLoading, $http) {

	return {
		cekLokasiSekarang: function(){
			$ionicPlatform.ready(function() {
				$ionicLoading.show({
					content: 'Getting current location...',
					showBackdrop: false
				});

				var posOptions = {timeout: 10000, enableHighAccuracy: false};

				$cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
					$ionicLoading.hide();

					var lat1 = position.coords.latitude;
					var long1 = position.coords.longitude;

				}, function(err) {
					$ionicLoading.hide();
					console.log(err);
				});		
			});
		},
	}
	
})



;