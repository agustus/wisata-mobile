/* 
* @Author: Dika
* @Date:   2016-06-07 07:19:41
* @Last Modified by:   Dika
* @Last Modified time: 2016-06-11 15:09:09
*/

appServices.factory('wisataKulinerServices', function($http, SETTINGS) {
	var baseUrl = SETTINGS.apiurl;

	return {
		ambilSemua: function(){
			return $http.get(baseUrl+'kuliner/ambilSemua'); 
		},
		ambilSatu: function(wisata_id){
			return $http.get(baseUrl+'kuliner/ambilSatu/'+wisata_id); 
		},
	    ambilGaleri: function(){
	      return $http.get(baseUrl+'galeri');
	    },
	    ambilSatuGaleri: function(wisata_id){
	      return $http.get(baseUrl+'kuliner/ambilGaleri/'+wisata_id);
	    },
	    ambilKomentar: function(wisata_id){
	      return $http.get(baseUrl+'kuliner/ambilKomentar/'+wisata_id);
	    },
		kirimKomentar: function(postData){
			return $http.post(baseUrl+'kuliner/kirimKomentar', postData);
		},
		hapusKomentar: function(postData){
			return $http.post(baseUrl+'kuliner/hapusKomentar', postData);
		},
		rating: function(postData){
			return $http.post(baseUrl+'kuliner/tambahRating', postData);
		},
		topWisata: function(){
			return $http.get(baseUrl+'kuliner/ambilTopWisata');
		}
	};
});